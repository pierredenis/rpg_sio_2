<?php

require_once 'Mage.php';
class MageBlanc extends Mage
{

    public function __construct($nom, $pv, $force, $mana)
    {
        parent::__construct($nom, $pv, $force, $mana);
    }

    public function soigner(Personnage $perso,$pv){
        $perso->addPv($pv+30);
    }

}