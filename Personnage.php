<?php


abstract class Personnage
{

    protected $nom;
    protected $pv;
    protected $force;

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return int
     */
    public function getPv()
    {
        return $this->pv;
    }

    /**
     * @return mixed
     */
    public function getForce()
    {
        return $this->force;
    }

    /**
     * @return mixed
     */
    public function getPvMax()
    {
        return $this->pvMax;
    }

    /**
     * @return bool
     */
    public function isVivant()
    {
        return $this->vivant;
    }
    private $pvMax;
    private $vivant;

    public function __construct($nom,$pv,$force)
    {
        $this->nom = $nom;
        $this->pv = $pv - mt_rand(10,90);
        $this->force = $force;
        $this->pvMax = $pv;
        $this->vivant = true;
    }

    public function addPv($pv){
        if($this->vivant){
            if($this->pv + $pv > $this->pvMax){
                $this->pv = $this->pvMax;
            }else{
                $this->pv += $pv;
            }
        }

    }

    public function soigner(Personnage $perso,$pv){
        $perso->addPv($pv);
    }

    public function subirDegats($pv){
        if($this->pv - $pv < 0){
            $this->pv = 0;
            $this->vivant = false;
        }else {
            $this->pv -= $pv;
        }
    }

    public function attaquer(Personnage $perso){
        $perso->subirDegats($this->force);
    }
}