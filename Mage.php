<?php

require_once 'Personnage.php';

class Mage extends Personnage
{
    protected $mana;

    public function __construct($nom, $pv, $force,$mana)
    {
        parent::__construct($nom, $pv, $force);
        $this->mana = $mana;
    }

    public function fireBall(personnage $perso){
        $perso->subirDegats(mt_rand(10,20));
    }

    public function attaquer(Personnage $perso)
    {
        $perso->subirDegats(mt_rand($this->force-3,$this->force));
    }

    public function volDeMana(Mage $mage){

    }

    public function soigner(Personnage $guerrier,$pv){
        $guerrier->addPv($pv+10);
    }

}