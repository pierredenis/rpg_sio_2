<?php

require_once 'Personnage.php';

class Guerrier extends Personnage {

    public function __construct($nom, $pv, $force)
    {
        parent::__construct($nom, $pv, $force);
    }
}